import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  items = [
    {
      date: '2018-04-11',
      distance: '5000.00',
      time: '42.40'
    }
  ]
  public inputField ={
    date: '',
    distance: '',
    time: ''

  }
  addItem(inputField: InputType){

  this.items.push({
    date: inputField.date,
    distance: inputField.distance,
    time: inputField.time })
  }
  constructor() { }

  ngOnInit(): void {
  }

}

export interface InputType {
  date: string;
  distance: string;
  time: string;
}
